<?php /* Template Name: home */ ?>

<!DOCTYPE>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width"/>
		<title>Заголовок</title>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/home.css" />
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/font-awesome-4.2.0/css/font-awesome.min.css" />
			


		<meta name="format-detection" content="telephone-no" />
	</head>
	<body class="Home">
<?php while(have_posts()){
	the_post();
}?>
		<header class="HomeHeader" style="background-image: url('http://localhost/bion_index/wp-content/uploads/2017/09/fon.png');">
				<div class="col-lg-12">
					<div class="HomeHeaderContainer">
						<div class="Logo">
							<img src="<?php echo get_field("logo"); ?>" alt="logo" height="55" width="100"/>
						</div>
						<div class="Nav">
						<button class="menu_button hidden-lg hidden-md"><i class="fa fa-bars" aria-hidden="true"></i></button>
							<ul>
								<li><a href="#">about us<img src="http://localhost/bion_index/wp-content/uploads/2017/09/arrow-down.svg" height="10" width="10"></a></li>
								<li><a href="#">breeding program<img src="http://localhost/bion_index/wp-content/uploads/2017/09/arrow-down.svg" height="10" width="10"></a></li>
								 <li><a href="#">stock-lict</a></li>
								<li><a href="#">useful info<img src="http://localhost/bion_index/wp-content/uploads/2017/09/arrow-down.svg" height="10" width="10"></a></li>
								<li><a href="#">contacts</a></li>
								<li><a href="#">request price-list</a></li>
							</ul>
						</div>
					</div>
					
					<div class="HomeHeader-hr"></div>
					<div class="HomeHeader-name">
						<span class="name"><?php the_field('intro_text1'); ?></span>
						<br>
						<span class="text"><?php the_field("intro_text2"); ?></span>
					</div>
					<div class="HomeHeader-line">
					</div>
				</div>
		</header>

		<section class="HomeCounters">
			<div class="HomeCounter">
				<span class="number">25</span>
				<div>
				<h6>years</h6><br>
				<p>expiriance in reptiles business</p>	
				</div>		
			</div>
			<div class="HomeCounter" >
				<span class="number">60</span>
				<div>
				<h6>people</h6><br>
				<p>on professinal stuff</p>	
				</div>		
			</div>
			<div class="HomeCounter">
				<span class="number">2000</span>
				<div>
				<h6>sq.m</h6><br>
				<p>for animals keeping</p>
				</div>			
			</div>
			<div class="HomeCounter" >
				<span class="number">1000</span>
				<div>
				<h6>spesiments</h6><br>
				<p>in stoke at one time</p>
				</div>
			</div>
			<div class="HomeCounter" >
				<span class="number">51</span>
				<div>
				<h6>countries</h6><br>
				<p>we are working with</p>
				</div>			
			</div>
			<div class="HomeCounters-more">	
				<button>more about bion</button>		
			</div>
		</section>

		<section class="HomeAdvantages" style="background-image: url('http://localhost/bion_index/wp-content/uploads/2017/09/leaves.png');">
		    <p>advantages</p>
			<div class="container">
					<div class="HomeAdvantagesWrapper">
					<ul class="HomeAdvantage-first">
						<li class="HomeAdvantage-item">
						
						</li>
						<li class="HomeAdvantage-item">
						</li>
						<li class="HomeAdvantage-item">
						</li>
						<li class="HomeAdvantage-item">
						</li>
					</ul>
			
					<ul class="HomeAdvantage-second">
						<li class="HomeAdvantage-item">
						</li>
						<li class="HomeAdvantage-item">
						</li>
						<li class="HomeAdvantage-item">
						</li>
						<li class="HomeAdvantage-item">
						</li>
					</ul>	
					</div>
					<div class="HomeAdvantage-but">
						<button>view ctock-list</button>
					</div>
			</div>
		</section>

		<section class="HomeDiscounts" style="background-image: url('http://localhost/bion_index/wp-content/uploads/2017/09/tree.png');" >
			<div class="HomeDiscountsBg">
			<div class="container">
				<div class="HomeDiscounts-title">
					<span>discounts</span><br>
					<p>All BION’s wholesale customers are entitled to special prices and discounts (depending on quantity) as well as privileged shipping cost, namely</p>
				</div>
				<div class="HomeDiscounts-percent">
					<div class="HomeDiscount-item">
						<span class="number">2.5%</span>
						<p>For all orders above $15,000</p>
					</div>
					<div class="HomeDiscount-item">
						<span class="number">5.0%</span>
						<p>For all orders above $20,000</p>
					</div>
					<div class="HomeDiscount-item">
						<span class="number">7.0%</span>
						<p>For all orders above $25,000</p>
					</div>
				</div>
				<div class="HomeDiscounts-but">
					<button>learn more</button>
				</div>
			</div>
			</div>
		</section>

		<section class="HomeNews" style="background-image: url('http://localhost/bion_index/wp-content/uploads/2017/09/leaves.png');" >
			<div class="container">
				<ul class="HomeNews-inner">
					<li class="HomeNews-title">
						<span>news</span>
						<a href="#">view all</a>
					</li>
					<?php 
					$query = new WP_Query(array('post_type' => 'news' ));
					while($query -> have_posts()){
						$query -> the_post();
					 ?>
					
					<li class="HomeNews-item">
						<span class="number"><?php the_field("number_new"); ?></span>
						<span class="mounth"><?php the_field("mounth_news"); ?></span>
						<div class="line"></div>
						<p><?php the_field("text_news"); ?></p>
					</li>
					<?php 
					}
					wp_reset_postdata();
					?>
				</ul>
			</div>
		</section>

		<section class="HomeWriteus" style="background-image: url('http://localhost/bion_index/wp-content/uploads/2017/09/tree2.png');" >
			<div class="container">
					<div class="HomeWriteus-title">
						<span>in case of any questions drop as a line</span>
					</div>
					<form>
						<input type="text" name="name" placeholder="Name/Company name" value="" id="name" >
						<select name="country" id="country">
							<option selected value="Select">Select your country

							<option value="Ukraine">Ukraine
							<option value="Germany">Germany
							<option value="USA">USA
						</select><br><br>
						<select name="status" id="status">
							<option selected value="Select">Select your status
							<option value="Ukraine">First
							<option value="Germany">Second
							</select>
						<input type="text" name="email" placeholder="Email" value="" id="email"><br>
						<textarea placeholder="Type a massage" cols="72" rows="3"></textarea><br>
						<button>submit</button>
					</form>
			</div>
		</section>
		<footer>
		</footer>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/common.js"></script>	
	
	</body>
</html>